#AI. Порграма має відповісти на питання чи є введений стрінг
#1 - номером телефону
#2 - email-ом
#3 - Іменем з ініціалами
#4 - Даними невідомого формату
#
#+380631112233 -> Phone
#bcdef@abc.efg -> email   3+ letters @ 3 letters. 3 letters
#Bill J.I. -> name   2 words
#something else -> unknown

input_string = input("Enter some string: ")

result = 'Unknown'

if input_string.startswith('+') and input_string[1:].isdigit() and len(input_string) == 13:
    result = 'Phone number'
    print(result)
elif '@' in input_string and '.' in input_string:
    parts = input_string.split('@')
    if len(parts) == 2:
        domain_parts = parts[1].split('.')
        if len(domain_parts) >= 2 and all(len(part) >= 3 for part in domain_parts):
            result = 'Email'
            print(result)
elif len((splited := input_string.split(' '))) == 2:
    _name = splited[0]
    _initials = splited[1]
    if _name.isalpha() and _name.capitalize() == _name:
        if _initials[-1] == '.' and _initials[-3] == '.' and _initials[0].isalpha() and _initials[0].upper() == _initials[0] and _initials[2].isalpha() and _initials[2].upper() == _initials[2]:
            result = 'Name'
            print(result)

else:
    print(result)