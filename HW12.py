#Доопрацюйте гру з заняття наступним чином:
#Для обʼєкту классу Гравець при створенні питати їмʼя гравця, зберігати на обʼєкті
#Розширте гру новими ігровими фігурами https://bigbangtheory.fandom.com/wiki/Rock,_Paper,_Scissors,_Lizard,_Spock. 
#Скорегуйте поведінку всіх ігрових фігур під нові правила.
#При оголошенні результатів гри повинно виводитись повідомлення"Гравець [імʼя гравця] переміг 
#[імʼя гравця] за допомогою [ігрова фігура]"


from random import choice

class BasePlayer:
    def __init__(self, name):
        self.name = name

    def get_figure(self, options):
        return self._get_figure(options)

    def _get_figure(self, options):
        raise NotImplementedError

class HumanPlayer(BasePlayer):
    def _get_figure(self, options):
        names = [obj.name for obj in options]
        while True:
            user_input = input(f'{self.name}, enter one of {names}: ')
            if user_input not in names:
                print('Wrong input')
                continue

            for obj in options:
                if obj.name == user_input:
                    return obj

class AIPlayer(BasePlayer):
    def _get_figure(self, options):
        ai_choice = choice(options)
        print(f'{self.name} chose {ai_choice.name}')
        return ai_choice

class BaseGameFigure:
    def __eq__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) == type(other)
    
    def __ne__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) != type(other)
    
    def __lt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) < type(other)

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) > type(other)

    def __ge__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) >= type(other)

    def __le__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        return type(self) <= type(other)

    def __str__(self):
        return self.name

class Rock(BaseGameFigure):
    name = 'Rock'

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        if type(other) == Scissors or type(other) == Lizard:
            return True
        else:
            return False

class Scissors(BaseGameFigure):
    name = 'Scissors'

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        if type(other) == Paper or type(other) == Lizard:
            return True
        else:
            return False

class Paper(BaseGameFigure):
    name = 'Paper'

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        if type(other) == Rock or type(other) == Spock:
            return True
        else:
            return False

class Lizard(BaseGameFigure):
    name = 'Lizard'

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        if type(other) == Spock or type(other) == Paper:
            return True
        else:
            return False

class Spock(BaseGameFigure):
    name = 'Spock'

    def __gt__(self, other):
        if not isinstance(other, BaseGameFigure):
            raise TypeError
        if type(other) == Scissors or type(other) == Rock:
            return True
        else:
            return False

class RSPGame:
    game_name = 'Rock Scissors Paper Lizard Spock'
    player1 = None
    player2 = None
    rules = [Scissors(), Paper(), Rock(), Lizard(), Spock()]

    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

    def play(self):
        print(f'{self.game_name} started for 1 time play')
        self._play()

    def _play(self):
        f1 = self.player1.get_figure(self.rules)
        f2 = self.player2.get_figure(self.rules)

        if f1 == f2:
            print('Draw')
        elif f1 > f2:
            print(f'Player {self.player1.name} wins with {f1}')
        else:
            print(f'Player {self.player2.name} wins with {f2}')

    def play_3_times(self):
        print(f'{self.game_name} started for 3 time play')
        for _ in range(3):
            self._play()

ai_player1 = AIPlayer('AI')
human_player1 = HumanPlayer('Human')

game = RSPGame(ai_player1, human_player1)

game.play()
