# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб". 
# Наповніть класи атрибутами та методами на свій розсуд.
# Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель". Виведіть на екран значення атрибутів обʼєктів

class Transport:
    def __init__(self, model, capacity, max_speed):
        self.model = model
        self.capacity = capacity
        self.max_speed = max_speed

    def get_info(self):
        print("Model:", self.model)
        print("Capacity:", self.capacity)
        print("Max Speed:", self.max_speed)

class Car(Transport):
    def __init__(self, model, capacity, max_speed, fuel_type):
        super().__init__(model, capacity, max_speed)
        self.fuel_type = fuel_type

    def get_info(self):
        super().get_info()
        print("Fuel Type:", self.fuel_type)

class Plane(Transport):
    def __init__(self, model, capacity, max_speed, wingspan):
        super().__init__(model, capacity, max_speed)
        self.wingspan = wingspan

    def get_info(self):
        super().get_info()
        print("Wingspan:", self.wingspan)

class Ship(Transport):
    def __init__(self, model, capacity, max_speed, length):
        super().__init__(model, capacity, max_speed)
        self.length = length

    def get_info(self):
        super().get_info()
        print("Length:", self.length)

car = Car("BMW", 4, 250, "petrol")
plane = Plane("Boeing 737", 150, 900, 38)
ship = Ship("Titanic", 3000, 42, 269)

car.get_info()
print()
plane.get_info()
print()
ship.get_info()
