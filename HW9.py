# Створіть файл lib.py, помістіть в нього допоміжні функції вашої програми "Касир". 
# Імпортуйте їх для основної функції в основний файл. Запустіть "Касир" з основного файлу
# Помістіть в lib.py декоратор для вимірювання часу.
# Імпортуйте декоратор в основний файл, задекоруйте основну функцію "Касир".

from lib import measure_execution_time, get_word_form

@measure_execution_time
def check_age(age):
    word_forms = ['рік', 'роки', 'років']
    word_form = get_word_form(age, word_forms)

    if '7' in str(age):
        return f"Вам {age} {word_form}, вам пощастить"
    elif 0 < age < 7:
        return f"Тобі ж {age} {word_form}, де твої батьки?"
    elif 7 < age < 16:
        return f"Тобі лише {age} років, а це фільм для дорослих!"
    elif 100 > age > 65:
        return f"Вам {age} {word_form}? Покажіть пенсійне посвідчення!"
    else:
        return f"Незважаючи на те, що вам {age} {word_form}, білетів всеодно нема!"

try:
    age = int(input("Enter your age: "))
except ValueError:
    print("Invalid input. Please enter a valid age as a number.")
else:
    result = check_age(age)
    print(result)






