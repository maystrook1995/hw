import time

def measure_execution_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Function '{func.__name__}' executed in {execution_time:.6f} seconds")
        return result
    return wrapper

def get_word_form(num, word_forms):
    if isinstance(num, int):
        last_digit = num % 10
        last_two_digits = num % 100

        if last_digit == 1 and last_two_digits != 11:
            return word_forms[0]
        elif last_digit in [2, 3, 4] and last_two_digits not in [12, 13, 14]:
            return word_forms[1]
        else:
            return word_forms[2]
    else:
        return word_forms[2]
    