#Підключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ), 
#отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
# "[дата, на яку актуальний курс]"
#1. [назва валюти 1] to UAH: [значення курсу валюти 1]
#2. [назва валюти 2] to UAH: [значення курсу валюти 2]
#3. [назва валюти 3] to UAH: [значення курсу валюти 3]
#...
#n. [назва валюти n] to UAH: [значення курсу валюти n]
#опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс
#P.S.За можливості зробіть все за допомогою ООП

import requests
import datetime

class RatesToTxt:
    def __init__(self, file_name='rates.txt'):
        self.file_name = file_name
        self.api_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

    def get_rates(self, date=None):
        if date is None:
            date = datetime.date.today()
        response = requests.get(self.api_url, params={'date': date.strftime('%Y%m%d')})
        rates = response.json() # requests auto-decodes json for us
        return rates

    def write_to_txt(self, rates):
        with open(self.file_name, 'w', encoding='utf-8') as f:
            f.write(f'Rates for {rates[0]["exchangedate"]}\n')
            for i, rate in enumerate(rates, start=1):
                f.write(f'{i}. {rate["txt"]} to UAH: {rate["rate"]}\n')

if __name__ == "__main__":
    rtt = RatesToTxt()
    rates = rtt.get_rates()
    rtt.write_to_txt(rates)