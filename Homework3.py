#1. Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові. 
#Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'". 
#Слово та номер символу отримайте за допомогою input() або скористайтеся константою. 
#Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".

word = input("Enter some word: ")
position = int(input("Enter number of symbol: "))

symbol = word[position - 1]

result = "The {} symbol in '{}' is '{}'.".format(position, word, symbol)
print(result)

#2. Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). 
#Напишіть код, який визначить кількість слів, в цих даних.

data = input("Enter some sentence: ")

word_count = len(data.split())

print("Number of words in line: ", word_count)

#3. Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. 
#Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1. 
#Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for x in lst1:
    try:
        num = float(x)
        lst2.append(num)
    except ValueError:
        pass
